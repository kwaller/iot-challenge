﻿using IoTChallenge.Events.Models;

namespace IoTChallenge.Events
{
    public class DeviceStateCreated : BaseEvent<DeviceStateCreatedPayload>
    {
        public DeviceStateCreated(string correlationId, Actor actor) : base(correlationId, actor)
        {
        }

        public override string Type => typeof(DeviceStateCreated).Name;
    }
}