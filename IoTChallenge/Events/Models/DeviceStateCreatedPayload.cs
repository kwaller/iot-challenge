﻿using System;

namespace IoTChallenge.Events.Models
{
    public class DeviceStateCreatedPayload
    {
        public decimal Latitude { get; set; }

        public decimal Longitude { get; set; }

        public long MotorSpeed { get; set; }

        public decimal Voltage { get; set; }
    }
}
