﻿using System;

namespace IoTChallenge.Events.Models
{
    public class DeviceCreatedPayload
    {
        public Guid SerialNumber { get; set; }
        public string Type { get; set; }
    }
}
