﻿using System;
using IoTChallenge.Events.Models;

namespace IoTChallenge.Events
{
    public class DeviceCreated : BaseEvent<DeviceCreatedPayload>
    {
        public DeviceCreated(string correlationId, Actor actor) : base(correlationId, actor)
        {
        }

        public override string Type => typeof(DeviceCreated).Name;
    }
}
