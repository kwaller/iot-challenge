﻿using System;

namespace IoTChallenge.Models.Request
{
    public class DeviceStateRequest
    {
        public decimal Latitude { get; set; }

        public decimal Longitude { get; set; }

        public long MotorSpeed { get; set; }

        public decimal Voltage { get; set; }
    }
}
