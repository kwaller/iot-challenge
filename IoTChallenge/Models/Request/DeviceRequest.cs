﻿using System;

namespace IoTChallenge.Models.Request
{
    public class DeviceRequest
    {
        public Guid SerialNumber { get; set; }
        public string Type { get; set; }
    }
}
