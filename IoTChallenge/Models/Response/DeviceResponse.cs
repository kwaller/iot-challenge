﻿using System;

namespace IoTChallenge.Models.Response
{
    public class DeviceResponse
    {
        public Guid Id { get; set; }
        public Guid SerialNumber { get; set; }
        public string Type {get;set;}
    }
}
