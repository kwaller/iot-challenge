﻿using System;

namespace IoTChallenge.Models.Response
{
    public class DeviceStateResponse
    {
        public Guid Id { get; set; }

        public Guid DeviceId { get; set; }

        public DateTime Timestamp { get; set; }

        public decimal Latitude { get; set; }

        public decimal Longitude { get; set; }

        public long MotorSpeed { get; set; }

        public decimal Voltage { get; set; }
    }
}
