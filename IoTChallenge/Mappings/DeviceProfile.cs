﻿using System;
using AutoMapper;
using IoTChallenge.Entities;
using IoTChallenge.Events.Models;
using IoTChallenge.Infrastructure;
using IoTChallenge.Models.Request;
using IoTChallenge.Models.Response;

namespace IoTChallenge.Mappings
{
    public class DeviceProfile : Profile
    {
        public DeviceProfile()
        {
            CreateMap<DeviceRequest, DeviceEntity>()
                .ForMember(dest => dest.PartitionKey, opts => opts.MapFrom(src => IoTChallengeConstants.DeviceCollectionId))
                .ForMember(dest => dest.RowKey, opts => opts.MapFrom(src => Guid.NewGuid().ToString()));
            CreateMap<DeviceEntity, DeviceResponse>()
                .ForMember(dest => dest.Id, opts => opts.MapFrom(src => src.RowKey));

            // events
            CreateMap<DeviceEntity, DeviceCreatedPayload>();
        }
    }
}
