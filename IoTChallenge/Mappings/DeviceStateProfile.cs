﻿using System;
using System.Globalization;
using AutoMapper;
using IoTChallenge.Entities;
using IoTChallenge.Events.Models;
using IoTChallenge.Infrastructure;
using IoTChallenge.Models.Request;
using IoTChallenge.Models.Response;

namespace IoTChallenge.Mappings
{
    public class DeviceStateProfile : Profile
    {
        public DeviceStateProfile()
        {
            CreateMap<DeviceStateRequest, DeviceStateEntity>()
                .ForMember(dest => dest.PartitionKey, opts => opts.MapFrom(src => IoTChallengeConstants.DeviceStateCollectionId))
                .ForMember(dest => dest.RowKey, opts => opts.MapFrom(src => Guid.NewGuid().ToString()))
                .ForMember(dest => dest.Latitude, opts => opts.MapFrom(src => src.Latitude.ToString(CultureInfo.InvariantCulture)))
                .ForMember(dest => dest.Longitude, opts => opts.MapFrom(src => src.Longitude.ToString(CultureInfo.InvariantCulture)))
                .ForMember(dest => dest.MotorSpeed, opts => opts.MapFrom(src => src.MotorSpeed.ToString(CultureInfo.InvariantCulture)))
                .ForMember(dest => dest.Voltage, opts => opts.MapFrom(src => src.Voltage.ToString(CultureInfo.InvariantCulture)));
            CreateMap<DeviceStateEntity, DeviceStateResponse>()
                .ForMember(dest => dest.Id, opts => opts.MapFrom(src => src.RowKey))
                .ForMember(dest => dest.Timestamp, opts => opts.MapFrom(src => src.Timestamp.ToString("o")))
                .ForMember(dest => dest.Latitude, opts => opts.MapFrom(src => decimal.Parse(src.Latitude)))
                .ForMember(dest => dest.Longitude, opts => opts.MapFrom(src => decimal.Parse(src.Longitude)))
                .ForMember(dest => dest.MotorSpeed, opts => opts.MapFrom(src => long.Parse(src.MotorSpeed)))
                .ForMember(dest => dest.Voltage, opts => opts.MapFrom(src => decimal.Parse(src.Voltage)));

            // events
            CreateMap<DeviceStateEntity, DeviceStateCreatedPayload>();
        }
    }
}
