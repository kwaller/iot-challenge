﻿namespace IoTChallenge.Infrastructure
{
    public static class IoTChallengeConstants
    {
        public const string ApiMimeType = "application/json";

        public const string EventTopicName = "IoT-Challenge";
        public const string DeviceCollectionId = "Device";
        public const string DeviceStateCollectionId = "DeviceState";
    }
}
