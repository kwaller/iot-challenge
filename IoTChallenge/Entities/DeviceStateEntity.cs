﻿using System;
using Microsoft.WindowsAzure.Storage.Table;

namespace IoTChallenge.Entities
{
    public class DeviceStateEntity : TableEntity
    {
        public Guid DeviceId { get; set; }

        public string Latitude { get; set; }

        public string Longitude { get; set; }

        public string MotorSpeed { get; set; }

        public string Voltage { get; set; }
    }
}
