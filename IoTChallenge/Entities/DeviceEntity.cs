﻿using System;
using IoTChallenge.Infrastructure;
using Microsoft.WindowsAzure.Storage.Table;

namespace IoTChallenge.Entities
{
    public class DeviceEntity : TableEntity
    {
        public Guid SerialNumber { get; set; }
        public string Type { get; set; }
    }
}
