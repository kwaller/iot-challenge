﻿using AutoMapper;
using IoTChallenge.Infrastructure;
using IoTChallenge.Mappings;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Serilog;
using Serilog.Events;
using Swashbuckle.AspNetCore.Swagger;
using System.Collections.Generic;
using System.Linq;
using IoTChallenge.Events;
using IoTChallenge.Events.Services;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.Extensions.Logging;
using Microsoft.WindowsAzure.Storage;

namespace IoTChallenge
{
    public class Startup
    {
        public IConfiguration Configuration { get; }

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public void ConfigureServices(IServiceCollection services)
        {
            Log.Logger = new LoggerConfiguration()
                .MinimumLevel.Debug()
                .MinimumLevel.Override("Microsoft", LogEventLevel.Information)
                .Enrich.FromLogContext()
                .ReadFrom.Configuration(Configuration)
                .CreateLogger();

            services
                .AddLogging(loggingBuilder =>
                {
                    loggingBuilder.AddSeq();
                });

            services
                .AddSwaggerGen(x =>
                {
                    x.SwaggerDoc("v1", new Info { Title = "IoT Challenge", Version = "v1" });

                    // TODO: get Swagger to hydrate the access token to the requests
                    //x.AddSecurityDefinition("Auth0", new OAuth2Scheme
                    //{
                    //    Type = "oauth2",
                    //    Flow = "implicit",
                    //    AuthorizationUrl = Configuration["Auth0:AuthorizationUrl"],
                    //    Scopes = new Dictionary<string, string>
                    //    {
                    //        { "oidc", "oidc" },
                    //        { "profile", "profile" }
                    //    }
                    //});
                    x.AddSecurityDefinition("Bearer", new ApiKeyScheme
                    {
                        In = "header",
                        Description = "Please enter JWT with Bearer into field",
                        Name = "Authorization",
                        Type = "apiKey"
                    });
                    x.AddSecurityRequirement(new Dictionary<string, IEnumerable<string>> {
                        { "Bearer", Enumerable.Empty<string>() }
                    });
                });

            services
                .AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
                .AddJwtBearer(options =>
                {
                    options.Authority = $"https://{Configuration["Auth0:Domain"]}";
                    options.Audience = Configuration["Auth0:Identifier"];
                })
                .AddCookie()
                .AddOpenIdConnect(options =>
                {
                    options.Authority = $"https://{Configuration["Auth0:Domain"]}";
                    options.ClientId = Configuration["Auth0:ClientId"];
                    options.ClientSecret = Configuration["Auth0:ClientSecret"];

                    options.Scope.Add("oidc");
                    options.Scope.Add("profile");
                    options.ResponseType = "access_token";
                    options.GetClaimsFromUserInfoEndpoint = true;

                    options.SignInScheme = CookieAuthenticationDefaults.AuthenticationScheme;

                    options.SaveTokens = true;
                });

            services
                .AddSingleton(Log.Logger)
                .AddSingleton(x =>
                {
                    return new MapperConfiguration(cfg =>
                    {
                        cfg.AddProfile(typeof(DeviceProfile));
                        cfg.AddProfile(typeof(DeviceStateProfile));
                    }).CreateMapper();
                })
                .AddSingleton(x =>
                {
                    var storageAccount = CloudStorageAccount.Parse(Configuration["Azure:StorageConnectionString"]);
                    var tableClient = storageAccount.CreateCloudTableClient();

                    var deviceTable = tableClient.GetTableReference(IoTChallengeConstants.DeviceCollectionId);
                    deviceTable.CreateIfNotExistsAsync().Wait();

                    var deviceStateTable = tableClient.GetTableReference(IoTChallengeConstants.DeviceStateCollectionId);
                    deviceStateTable.CreateIfNotExistsAsync();

                    return tableClient;
                })
                .AddSingleton<IEventOptions>(x => new EventOptions
                {
                    AzureServiceBusConnectionString = Configuration["Azure:ServiceBusConnectionString"],
                    TopicName = IoTChallengeConstants.EventTopicName
                })
                .AddSingleton<IEventPublisher, EventPublisher>();

            services.AddCors();
            services.AddMvc();
        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {
            loggerFactory.AddSerilog();

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseSwagger();
            app.UseSwaggerUI(x =>
            {
                x.SwaggerEndpoint("/swagger/v1/swagger.json", "IoT Challenge");
                x.OAuthAppName("Auth0");
                x.OAuthClientId(Configuration["Auth0:ClientId"]);
                x.OAuthUseBasicAuthenticationWithAccessCodeGrant();
            });

            app.UseAuthentication();
            app.UseCors(x =>
            {
                x.AllowAnyHeader();
                x.AllowAnyMethod();
                x.AllowAnyOrigin();
            });
            app.UseMvc();

        }
    }
}
