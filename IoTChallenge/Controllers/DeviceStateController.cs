﻿using AutoMapper;
using IoTChallenge.Infrastructure;
using IoTChallenge.Models.Request;
using IoTChallenge.Models.Response;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using IoTChallenge.Entities;
using IoTChallenge.Events;
using IoTChallenge.Events.Models;
using IoTChallenge.Events.Services;
using Microsoft.WindowsAzure.Storage.Table;

namespace IoTChallenge.Controllers
{
    [Authorize]
    [Route("api/device/{deviceId:guid}/state")]
    [Produces(IoTChallengeConstants.ApiMimeType)]
    [ProducesResponseType(401)]
    [ProducesResponseType(500)]
    public class DeviceStateController : Controller
    {
        private CloudTable DeviceTable { get; }
        private CloudTable DeviceStateTable { get; }
        private IEventPublisher EventPublisher { get; }
        private IMapper Mapper { get; }

        public DeviceStateController(
            CloudTableClient tableClient,
            IEventPublisher eventPublisher,
            IMapper mapper)
        {
            EventPublisher = eventPublisher;
            Mapper = mapper;

            DeviceTable = tableClient.GetTableReference(IoTChallengeConstants.DeviceCollectionId);
            DeviceStateTable = tableClient.GetTableReference(IoTChallengeConstants.DeviceStateCollectionId);
        }
        
        [HttpPost]
        [ProducesResponseType(typeof(DeviceStateResponse), 200)]
        [ProducesResponseType(400)]
        public async Task<IActionResult> Create(Guid deviceId, [FromBody] DeviceStateRequest request)
        {
            if (await DeviceExists(deviceId))
            {
                var deviceState = Mapper.Map<DeviceStateEntity>(request);
                deviceState.DeviceId = deviceId;

                var result = await DeviceStateTable.ExecuteAsync(TableOperation.Insert(deviceState));
                if (result.HttpStatusCode >= 200 && result.HttpStatusCode < 300)
                {
                    await EventPublisher.PublishEvent<DeviceStateCreated, DeviceStateCreatedPayload>(
                        new DeviceStateCreated(HttpContext.TraceIdentifier, new Actor
                        {
                            Id = User.Claims.FirstOrDefault(x => x.Type == ClaimTypes.NameIdentifier)?.Value

                        })
                        {
                            Payload = Mapper.Map<DeviceStateCreatedPayload>(result.Result)
                        }
                    );

                    return Ok(Mapper.Map<DeviceStateResponse>(result.Result));
                }

                return StatusCode(result.HttpStatusCode);
            }

            return BadRequest();
        }

        [HttpGet]
        [ProducesResponseType(typeof(DeviceStateResponse), 200)]
        [ProducesResponseType(400)]
        [ProducesResponseType(404)]
        [Route("{stateId:guid}")]
        public async Task<IActionResult> Get(Guid deviceId, Guid stateId)
        {
            var deviceState = new List<DeviceStateEntity>();

            var deviceQuery = new TableQuery<DeviceStateEntity>()
                .Where(TableQuery.CombineFilters(
                    TableQuery.GenerateFilterCondition("DeviceId", QueryComparisons.Equal, deviceId.ToString()),
                    TableOperators.And,
                    TableQuery.GenerateFilterCondition("RowKey", QueryComparisons.Equal, stateId.ToString()))
                );

            TableContinuationToken token = null;
            deviceQuery.TakeCount = 1;

            if (await DeviceExists(deviceId))
            {
                do
                {
                    var segment = await DeviceTable.ExecuteQuerySegmentedAsync(deviceQuery, token);
                    token = segment.ContinuationToken;
                    foreach (var entity in segment)
                    {
                        deviceState.Add(entity);
                    }
                }
                while (token != null);
            }

            return Ok(Mapper.Map<DeviceStateResponse>(deviceState.FirstOrDefault()));
        }

        [HttpGet]
        [ProducesResponseType(typeof(IEnumerable<DeviceStateResponse>), 200)]
        [ProducesResponseType(404)]
        public async Task<IActionResult> GetAll(Guid deviceId)
        {
            var deviceState = new List<DeviceStateEntity>();

            var deviceQuery = new TableQuery<DeviceStateEntity>()
                .Where(TableQuery.GenerateFilterCondition("DeviceId", QueryComparisons.Equal, deviceId.ToString()));

            TableContinuationToken token = null;
            deviceQuery.TakeCount = 100;

            if (await DeviceExists(deviceId))
            {
                do
                {
                    var segment = await DeviceTable.ExecuteQuerySegmentedAsync(deviceQuery, token);
                    token = segment.ContinuationToken;
                    foreach (var entity in segment)
                    {
                        deviceState.Add(entity);
                    }
                }
                while (token != null);
            }

            return Ok(Mapper.Map<IEnumerable<DeviceStateResponse>>(deviceState));
        }

        private async Task<bool> DeviceExists(Guid deviceId)
        {
            var devices = new List<DeviceEntity>();

            var deviceQuery = new TableQuery<DeviceEntity>()
                .Where(TableQuery.GenerateFilterCondition("RowKey", QueryComparisons.Equal, deviceId.ToString()));

            TableContinuationToken token = null;
            deviceQuery.TakeCount = 10;
            do
            {
                var segment = await DeviceTable.ExecuteQuerySegmentedAsync(deviceQuery, token);
                token = segment.ContinuationToken;
                foreach (var entity in segment)
                {
                    devices.Add(entity);
                }
            }
            while (token != null);

            return devices.Any();
        }
    }
}
