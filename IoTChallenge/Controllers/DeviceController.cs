﻿using IoTChallenge.Infrastructure;
using IoTChallenge.Models.Request;
using IoTChallenge.Models.Response;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using AutoMapper;
using IdentityModel;
using IoTChallenge.Entities;
using IoTChallenge.Events;
using IoTChallenge.Events.Models;
using IoTChallenge.Events.Services;
using Microsoft.WindowsAzure.Storage.Table;

namespace IoTChallenge.Controllers
{
    [Authorize]
    [Route("api/device")]
    [Produces(IoTChallengeConstants.ApiMimeType)]
    [ProducesResponseType(401)]
    [ProducesResponseType(500)]
    public class DeviceController : Controller
    {
        private CloudTable DeviceTable { get; }
        private IEventPublisher EventPublisher { get; }
        private IMapper Mapper { get; }

        public DeviceController(
            CloudTableClient tableClient,
            IEventPublisher eventPublisher,
            IMapper mapper)
        {
            EventPublisher = eventPublisher;
            Mapper = mapper;

            DeviceTable = tableClient.GetTableReference(IoTChallengeConstants.DeviceCollectionId);
        }

        [HttpPost]
        [ProducesResponseType(typeof(DeviceResponse), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(DeviceResponse), 409)]
        public async Task<IActionResult> Create([FromBody] DeviceRequest request)
        {
            var devices = new List<DeviceEntity>();

            var deviceQuery = new TableQuery<DeviceEntity>()
                .Where(TableQuery.GenerateFilterCondition("SerialNumber", QueryComparisons.Equal, request.SerialNumber.ToString()));

            TableContinuationToken token = null;
            deviceQuery.TakeCount = 10;
            do
            {
                var segment = await DeviceTable.ExecuteQuerySegmentedAsync(deviceQuery, token);
                token = segment.ContinuationToken;
                foreach (var entity in segment)
                {
                    devices.Add(entity);
                }
            }
            while (token != null);

            if (devices.Any())
            {
                return StatusCode(409, Mapper.Map<DeviceResponse>(devices.FirstOrDefault()));
            }

            var device = Mapper.Map<DeviceEntity>(request);

            var result = await DeviceTable.ExecuteAsync(TableOperation.Insert(device));
            if (result.HttpStatusCode >= 200 && result.HttpStatusCode < 300)
            {
                await EventPublisher.PublishEvent<DeviceCreated, DeviceCreatedPayload>(
                    new DeviceCreated(HttpContext.TraceIdentifier, new Actor
                    {
                        Id = User.Claims.FirstOrDefault(x => x.Type == ClaimTypes.NameIdentifier)?.Value

                    })
                    {
                        Payload = Mapper.Map<DeviceCreatedPayload>(result.Result)
                    }
                );

                return Ok(Mapper.Map<DeviceResponse>(result.Result));
            }

            return StatusCode(result.HttpStatusCode);
        }

        [HttpGet]
        [ProducesResponseType(typeof(DeviceResponse), 200)]
        [ProducesResponseType(404)]
        [Route("{deviceId:guid}")]
        public async Task<IActionResult> Get(Guid deviceId)
        {
            var devices = new List<DeviceEntity>();

            var deviceQuery = new TableQuery<DeviceEntity>()
                .Where(TableQuery.GenerateFilterCondition("RowKey", QueryComparisons.Equal, deviceId.ToString()));

            TableContinuationToken token = null;
            deviceQuery.TakeCount = 10;
            do
            {
                var segment = await DeviceTable.ExecuteQuerySegmentedAsync(deviceQuery, token);
                token = segment.ContinuationToken;
                foreach (var entity in segment)
                {
                    devices.Add(entity);
                }
            }
            while (token != null);

            if (devices.Any())
            {
                return Ok(Mapper.Map<DeviceResponse>(devices.Any()));
            }

            return NotFound();
        }

    }
}
