﻿using System.Threading.Tasks;
using IoTChallenge.Events.Models;

namespace IoTChallenge.Events.Services
{
    public interface IEventPublisher
    {
        Task PublishEvent<TEvent, TPayload>(TEvent @event) where TEvent : IEvent<TPayload>;
    }
}
