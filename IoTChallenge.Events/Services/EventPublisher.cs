﻿using System;
using System.Text;
using System.Threading.Tasks;
using IoTChallenge.Events.Models;
using Microsoft.Azure.ServiceBus;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Serilog;

namespace IoTChallenge.Events.Services
{
    public class EventPublisher : IEventPublisher
    {
        public IEventOptions Options { get; }
        public ILogger Logger { get; }

        private TopicClient TopicClient { get; }
        
        public EventPublisher(
            IEventOptions options,
            ILogger logger)
        {
            Options = options;
            Logger = logger;

            Logger.Information("Event Publisher: initializing client for {EventTopic}",
                Options.TopicName);

            TopicClient = new TopicClient(options.AzureServiceBusConnectionString, options.TopicName);
        }
        
        public async Task PublishEvent<TEvent, TPayload>(TEvent @event) where TEvent : IEvent<TPayload>
        {
            Logger.Debug("Event {CorrelationId}: preparing event {EventType}",
                @event.CorrelationId,
                @event.Type);

            var messageId = Guid.NewGuid();

            var jEvent = JToken.FromObject(@event);
            jEvent[nameof(IEvent<object>.Payload)].Parent.Remove();

            var msg = new Message(Encoding.UTF8.GetBytes(jEvent.ToString()))
            {
                ContentType = "application/json",
                Label = typeof(TEvent).Name.ToString(),
                MessageId = messageId.ToString(),
                TimeToLive = TimeSpan.FromDays(1)
            };

            msg.MessageId = messageId.ToString();
            msg.CorrelationId = @event.CorrelationId.ToString();

            msg.UserProperties[nameof(Actor.Id)] = @event.Actor.Id;
            msg.UserProperties[nameof(BaseEvent<TEvent>.Type)] = @event.Type;
            msg.UserProperties[nameof(BaseEvent<TEvent>.Payload)] = JsonConvert.SerializeObject(@event.Payload);

            await TopicClient.SendAsync(msg);

            Logger.Information("Event {CorrelationId}: publish successful", @event.CorrelationId);
        }

    }
}
