﻿using System;

namespace IoTChallenge.Events.Models
{
    [Serializable]
    public abstract class BaseEvent<TPayload> : IEvent<TPayload>
    {
        public Actor Actor { get; }

        public string CorrelationId { get; }
        public abstract string Type { get; }

        public DateTime Timestamp { get; set; } = DateTime.UtcNow;
        public TPayload Payload { get; set; }

        protected BaseEvent(string correlationId, Actor actor)
        {
            CorrelationId = correlationId;
            Actor = actor;
        }
    }
}