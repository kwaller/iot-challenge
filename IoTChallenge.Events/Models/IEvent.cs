﻿using System;

namespace IoTChallenge.Events.Models
{
    public interface IEvent<TPayload>
    {
        Actor Actor { get; }

        string CorrelationId { get; }
        string Type { get; }
        DateTime Timestamp { get; set; }

        TPayload Payload { get; set; }
    }
}
