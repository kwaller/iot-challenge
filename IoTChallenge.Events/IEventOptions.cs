﻿namespace IoTChallenge.Events
{
    public interface IEventOptions
    {
        string AzureServiceBusConnectionString { get; set; }
        string TopicName { get; set; }
    }
}
