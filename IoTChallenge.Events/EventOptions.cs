﻿namespace IoTChallenge.Events
{
    public class EventOptions : IEventOptions
    {
        public string AzureServiceBusConnectionString { get; set; }
        public string TopicName { get; set; }
    }
}
